import React from 'react'
import IconButton from '../template/iconButton';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { markAsDone, markAsPending, remove } from './todoActions'

const TotoList = props => {

    const renderRow = () => {

        const list = props.list || []

        return list.map(ret => (
            <tr key={ret._id}>
                <td className={ret.done ? 'markedAsDone' : ''}>
                    {ret.description}
                </td>
                <td>
                    <IconButton style="success" icon="check" onClick={() => props.markAsDone(ret)} hide={ret.done}></IconButton>
                    <IconButton style="warning" icon="undo" onClick={() => props.markAsPending(ret)} hide={!ret.done}></IconButton>
                    <IconButton style="danger" icon="trash-o" onClick={() => props.remove(ret)} hide={!ret.done}></IconButton>
                </td>
            </tr>
        ))
    }

    return (
        <table className="table">
            <thead>
                <tr>
                    <th>
                        Descrição
                    </th>
                    <th className="tableActions">
                        Ações
                    </th>
                </tr>
            </thead>
            <tbody>
                {renderRow()}
            </tbody>
        </table>
    )
}

const mapStateToProps = (state) => ({ list: state.todo.list })
const mapDispatchToProps = (dispatch) => bindActionCreators({ markAsDone, markAsPending, remove }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(TotoList)