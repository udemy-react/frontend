import React, { Component } from 'react'
import PageHeader from '../template/pageheader'
import TodoForm from './todoform'
import TodoList from './todolist'

import axios from "axios";

const URL = 'http://localhost:3003/api/todos'

export default class Todo extends Component {

    constructor(props) {
        super(props)

        this.state = { description: '', list: [] }

        this.handleAdd = this.handleAdd.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
        this.handleMarkAsDone = this.handleMarkAsDone.bind(this);
        this.handleMarkAsPending = this.handleMarkAsPending.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.handleClear = this.handleClear.bind(this);

        this.refresh();
    }

    refresh(description = '') {

        const search = description ? `&description__regex=/${description}/` : ''

        axios.get(`${URL}?sort=-createdAt${search}`).then((ret) => {
            this.setState({ ...this.state, description, list: ret['data'] })
        })
    }

    handleMarkAsDone(ret) {
        axios.put(`${URL}/${ret._id}`, { ...ret, done: true }).then((ret) => {
            this.refresh(this.state.description);
        })
    }

    handleMarkAsPending(ret) {
        axios.put(`${URL}/${ret._id}`, { ...ret, done: false }).then((ret) => {
            this.refresh(this.state.description);
        })
    }

    handleRemove(ret) {
        axios.delete(`${URL}/${ret._id}`).then((ret) => {
            this.refresh(this.state.description);
        })
    }

    handleChange(e) {
        this.setState({ ...this.state, description: e.target.value })
    }

    handleSearch() {
        this.refresh(this.state.description)
    }

    handleClear() {
        this.refresh();
    }

    handleAdd() {
        const description = this.state.description

        axios.post(URL, { description }).then((ret) => {
            this.refresh();
        })

    }

    render() {
        return (
            <div>
                <PageHeader name="tarefas" small="Cadastro"></PageHeader>

                <TodoForm
                    handleChange={this.handleChange}
                    handleClear={this.handleClear}
                    handleSearch={this.handleSearch}
                    handleAdd={this.handleAdd} />

                <TodoList
                    handleMarkAsDone={this.handleMarkAsDone}
                    handleMarkAsPending={this.handleMarkAsPending}
                    handleRemove={this.handleRemove}
                />
            </div>
        )
    }
}